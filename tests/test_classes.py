"""Testing the data making classes."""
import unittest
from datetime import timedelta
from hypothesis import given, settings
from hypothesis.strategies import integers

from simplemath.src.classes import (
    BinaryArithmeticData,
    LinearPolynomialData,
    ExpDecompArithmeticData,
    InequalityData,
    BinaryArithmeticInequalityData,
    SimpleMultiplicationData
)
MAX_SECONDS = 26  # heroku has a thirty second timeout


class TestData(unittest.TestCase):
    """Testing the data-making classes."""
    @given(
        num_observations=integers(min_value=0, max_value=100000),
        low=integers(max_value=100),
        high=integers(min_value=101)
    )
    @settings(deadline=timedelta(seconds=MAX_SECONDS))
    def test_binaryarithmeticdata(self, num_observations, low, high):
        """Test BinaryArithmeticData."""
        arith = BinaryArithmeticData(
            num_observations=num_observations,
            low=low,
            high=high
        )
        self.assertEqual(len(arith.observations), num_observations)

    @given(
        num_observations=integers(min_value=0, max_value=100000),
        base=integers(min_value=2, max_value=32),
        low=integers(min_value=2, max_value=100),
        high=integers(min_value=101)
    )
    @settings(deadline=timedelta(seconds=MAX_SECONDS))
    def test_expdecompdata(self, num_observations, base, low, high):
        """Test ExpDecompArithmeticData."""
        expdecomp = ExpDecompArithmeticData(
            base=base,
            num_observations=num_observations,
            low=low,
            high=high
        )
        self.assertEqual(len(expdecomp.observations), num_observations)

    @given(
        num_observations=integers(min_value=0, max_value=100),
        lhs_length=integers(min_value=1, max_value=15),
        low=integers(max_value=100),
        high=integers(min_value=101)
    )
    @settings(deadline=timedelta(seconds=MAX_SECONDS))
    def test_linearpolynomialdata(self, num_observations, lhs_length, low, high):
        """Test LinearPolynomialData."""
        alg = LinearPolynomialData(
            lhs_length=lhs_length,
            num_observations=num_observations,
            low=low,
            high=high
        )
        self.assertEqual(len(alg.observations), num_observations)

    @given(
        num_observations=integers(min_value=0, max_value=100000),
        low=integers(max_value=100),
        high=integers(min_value=101)
    )
    @settings(deadline=timedelta(seconds=MAX_SECONDS))
    def test_inequalitydata(self, num_observations, low, high):
        """Test InequalityData."""
        ineq = InequalityData(
            num_observations=num_observations,
            low=low,
            high=high
        )
        self.assertEqual(len(ineq.observations), num_observations)

    @given(
        num_observations=integers(min_value=0, max_value=100000),
        low=integers(max_value=100),
        high=integers(min_value=101)
    )
    @settings(deadline=timedelta(seconds=MAX_SECONDS))
    def test_binary_arithmetic_inequalitydata(self, num_observations, low, high):
        """Test BinaryArithmeticInequalityData."""
        ineq = BinaryArithmeticInequalityData(
            num_observations=num_observations,
            low=low,
            high=high
        )
        self.assertEqual(len(ineq.observations), num_observations)

    @given(
        num_observations=integers(min_value=0, max_value=100000)
    )
    @settings(deadline=timedelta(seconds=MAX_SECONDS))
    def test_simple_multiplication_data(self, num_observations):
        """Test SimpleMultiplicationData."""
        smult = SimpleMultiplicationData(
            num_observations=num_observations,
        )
        self.assertEqual(len(smult.observations), num_observations)
