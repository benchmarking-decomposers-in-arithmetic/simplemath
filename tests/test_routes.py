"""Testing the routes."""
import unittest
from simplemath.main import APP


class TestRoutes(unittest.TestCase):
    """Testing routes
    """

    def setUp(self) -> None:
        """Runs prior to these tests."""

        APP.config["TESTING"] = True
        APP.config["WTF_CSRF_ENABLED"] = False

        self.app = APP.test_client()

    def test_binaryarithmetic_success(self) -> None:
        """Verifies a success on the binaryarithmeticdata route."""
        url = "/binaryarithmeticdata?num_observations=17&omit_multiplication=T"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 200)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertEqual(len(body["observations"]), 17)

    def test_binaryarithmetic_failure1(self) -> None:
        """Verifies a failure on the binaryarithmeticdata route."""
        url = "/binaryarithmeticdata?low=1000&high=1"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 400)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertIn("error", body.keys())

    def test_expdecomposition_success(self) -> None:
        """Verifies a success on the expdecompdata route."""
        url = "/expdecompdata?num_observations=17&base=2"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 200)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertEqual(len(body["observations"]), 17)

    def test_expdecomposition_failure1(self) -> None:
        """Verifies a failure on the expdecompdata route."""
        url = "/expdecompdata?pizza=1000&high=10000"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 400)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertIn("error", body.keys())

    def test_linearpolynomial_success(self) -> None:
        """Verifies a success on the linearpolynomialdata route."""
        url = "/linearpolynomialdata?num_observations=3&lhs_length=3"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 200)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertEqual(len(body["observations"]), 3)

    def test_linear_polynomial_failure1(self) -> None:
        """Verifies a failure on the linearpolynomialdata route."""
        url = "/linearpolynomialdata?num_observations=1000000"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 400)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertIn("error", body.keys())

    def test_inequality_success(self) -> None:
        """Verifies a success on the inequalitydata route."""
        url = "/inequalitydata?num_observations=3"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 200)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertEqual(len(body["observations"]), 3)

    def test_inequality_failure1(self) -> None:
        """Verifies a failure on the inequalitydata route."""
        url = "/inequalitydata?num_observations=1000000"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 400)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertIn("error", body.keys())

    def test_binaryarithmetic_inequality_success(self) -> None:
        """Verifies a success on the binaryarithmeticinequalitydata route."""
        url = "/binaryarithmeticinequalitydata?num_observations=3"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 200)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertEqual(len(body["observations"]), 3)

    def test_binaryarithmetic_inequality_failure1(self) -> None:
        """Verifies a failure on the binaryarithmeticinequalitydata route."""
        url = "/binaryarithmeticinequalitydata?num_observations=1000000"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 400)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertIn("error", body.keys())

    def test_simple_multiplication_success(self) -> None:
        """Verifies a success on the simplemultiplicationdata route."""
        url = "/simplemultiplicationdata?num_observations=3&sep=!"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 200)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertEqual(len(body["observations"]), 3)

    def test_simple_multiplication_failure1(self) -> None:
        """Verifies a failure on the simplemultiplicationdata route."""
        url = "/simplemultiplicationdata?num_observations=1000000"
        response = self.app.get(url)
        self.assertEqual(response.status_code, 400)
        body = response.json
        self.assertIsInstance(body, dict)
        self.assertIn("error", body.keys())
