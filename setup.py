import os
import io
from setuptools import setup, find_packages, find_namespace_packages  # type: ignore
from typing import List

__version__ = "0.1"
__author__ = "Quinn Dougherty"

def read_file(file_name: str) -> List[str]:
    """
    File read wrapper for loading data unmodified from arbitrary file.
    """
    file_path = os.path.join(os.path.dirname(__file__), file_name)
    with io.open(file_path, "r") as fin:
        return [line for line in fin if not line.startswith(("#", "--"))]

setup(
    name="simplemath",
    version=__version__,
    author=__author__,
    author_email="quinn.dougherty92@gmail.com",
    description="Make datasets for seq2seq arithmetic & algebra learning.",
    long_description=io.open("README.md").read(),
    long_description_content_type="application/markdown",
    url="https://gitlab.com/quinn-dougherty/simplemath",
    package_dir={'': 'simplemath'},
    packages=find_namespace_packages(where='simplemath', exclude=('test')),
    install_requires=read_file("requirements.txt"),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # setup_requires=["setuptools_scm", "wheel"],
    python_requires='>=3.7',
    entry_points = {
        "console_scripts": ["simplemath-dev=simplemath:start"],
    },
    keywords=[],
    include_package_data=True,
)
