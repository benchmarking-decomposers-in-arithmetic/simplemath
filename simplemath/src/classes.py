"""Data makers."""
from typing import Tuple
from random import choice, randrange
from math import log, ceil
from sympy import solveset, var  # type: ignore
from sympy.core.add import Add  # type: ignore
from sympy.core.symbol import Symbol  # type: ignore
from .abstract_base import DataBase

a, b, c, d, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z = var(
    "a b c d j k l m n o p q r s t u v w x y z"
)
variables = [a, b, c, d, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z]


class SimpleMultiplicationData(DataBase):
    """EXAMPLE: 10*2<SEP>20 or 3*4=12."""

    def __init__(
            self,
            num_observations: int,
            sep: str = "<SEP>",
    ) -> None:
        """Initialize class."""
        super().__init__(num_observations)
        self.sep = sep

    @staticmethod
    def make_lhs() -> str:
        """Produce the lhs of the observation."""
        if randrange(start=0, stop=2):  # a fair coin
            lhs = f"10*{randrange(start=0, stop=1000)}"
        else:
            lhs = f"{randrange(start=0, stop=10)}*{randrange(start=0, stop=10)}"
        return lhs

    def make_observation(self) -> str:
        """Produce a single sample."""
        lhs = self.make_lhs()
        return f"{lhs}{self.sep}{eval(lhs)}"


class BinaryArithmeticData(DataBase):
    """EXAMPLE: Evaluate 635*244.<SEP>635*244=154940."""

    def __init__(
            self,
            num_observations: int,
            sep: str = "<SEP>",
            low: int = 0,
            high: int = 1000,
            omit_division: bool = True,
            omit_multiplication: bool = False,
            omit_subtraction: bool = False,
            tearse: bool = True
    ) -> None:
        """Initialize class."""
        super().__init__(num_observations)
        self.ops = ["+", "-", "*", "/"]
        if omit_division:
            self.ops.pop(3)
        if omit_multiplication:
            self.ops.pop(2)
        if omit_subtraction:
            self.ops.pop(1)
        self.sep = sep
        self.low = low
        self.high = high
        self.tearse = tearse

    def make_lhs(self, operation: str) -> str:
        """Given an operation, pick two numbers and form the tree."""
        left = randrange(start=self.low, stop=self.high)
        right = randrange(start=self.low, stop=self.high)
        return f"{left}{operation}{right}"

    @staticmethod
    def make_rhs(lhs: str) -> str:
        """Given an expression, show the result.

        This method uses `eval`.
        """
        try:
            result = eval(lhs)
        except ZeroDivisionError:
            result = "undefined"
        return str(result)

    def make_observation(self) -> str:
        """Create a single sample."""
        operation = choice(self.ops)
        lhs = self.make_lhs(operation)
        rhs = self.make_rhs(lhs)
        if operation == "/":
            reflexive = "~"
        else:
            reflexive = "="
        if self.tearse:
            return f"{lhs}{self.sep}{rhs}"
        return f"Evaluate {lhs}.{self.sep}{lhs}{reflexive}{rhs}"


class InequalityData(DataBase):
    """EXAMPLE: 4<5<SEP>True."""

    def __init__(
            self,
            num_observations: int,
            low: int = 0,
            high: int = 100,
            sep: str = "<SEP>"
    ) -> None:
        """Initialize the class."""
        super().__init__(num_observations)
        self.low = low
        self.high = high
        self.sep = sep
        self.operators = ["<", ">"]

    def make_observation(self) -> str:
        """Create a single sample."""
        left = randrange(start=self.low, stop=self.high)
        right = randrange(start=self.low, stop=self.high)
        operator = choice(self.operators)
        lhs = f"{left}{operator}{right}"
        rhs = eval(lhs)
        return f"{lhs}{self.sep}{rhs}"


class BinaryArithmeticInequalityData(DataBase):
    """EXAMPLE: 4*5<9-4<SEP>False."""

    def __init__(
            self,
            num_observations: int,
            low: int = 0,
            high: int = 100,
            sep: str = "<SEP>",
            omit_division: bool = True
    ) -> None:
        """Initialize the class."""
        super().__init__(num_observations)
        self.low = low
        self.high = high
        self.sep = sep
        self.comparators = ["<", ">"]
        self.operators = ["+", "-", "*", "/"]
        if omit_division:
            self.operators.pop(3)

    def make_observation(self) -> str:
        """Create a single sample."""
        left_left = randrange(start=self.low, stop=self.high)
        left_right = randrange(start=self.low, stop=self.high)
        right_left = randrange(start=self.low, stop=self.high)
        right_right = randrange(start=self.low, stop=self.high)
        left_operator = choice(self.operators)
        right_operator = choice(self.operators)
        comparator = choice(self.comparators)
        lhs = (f"{left_left}{left_operator}{left_right}{comparator}"
               f"{right_left}{right_operator}{right_right}")
        rhs = eval(lhs)
        return f"{lhs}{self.sep}{rhs}"


class ExpDecompArithmeticData(DataBase):
    """EXAMPLE: Decompose 254 in base 10<SEP>254=2(10^2)+5(10^1)+4(10^0)."""

    def __init__(
            self,
            base: int,
            num_observations: int,
            low: int = 10,
            high: int = 1000000,
            sep: str = "<SEP>"
    ) -> None:
        """Initialize the class."""
        super().__init__(num_observations)
        self.base = base
        self.low = low
        self.high = high
        self.sep = sep

    def make_observation(self) -> str:
        """Create a single sample."""
        target = randrange(start=self.low, stop=self.high)
        length = ceil(log(target, self.base))
        string = f"Decompose {target} in base {self.base}{self.sep}{target}="
        summands = list()
        for power in range(length):
            place = self.base ** (power + 1)
            remainder = target % place
            quantity = remainder // (place // self.base)
            summands.append(f"{quantity}({self.base}^{power})")
            target -= remainder
        return string + "+".join(summands[::-1])


class LinearPolynomialData(DataBase):
    """EXAMPLE: Given 414*k + 751*s = 0, solve for k.<SEP>k is in [-751*s/414]."""

    def __init__(
            self,
            lhs_length: int,
            num_observations: int,
            sep: str = "<SEP>",
            low: int = 0,
            high: int = 10000
    ) -> None:
        """Initialize the class."""
        super().__init__(num_observations)
        self.lhs_length = lhs_length
        self.sep = sep
        self.low = low
        self.high = high
        self.variables = variables

    def make_eq(self, summands: int) -> Tuple[Add, Symbol]:
        """Form a Sympy expression. Implicitly this expression is set to zero."""
        symbols = [choice(self.variables)]
        expr = randrange(start=self.low, stop=self.high) * symbols[0]
        for _ in range(summands - 1):
            symbol = choice(self.variables)
            expr += randrange(start=self.low, stop=self.high) * symbol
            symbols.append(symbol)

        return expr, choice(symbols)

    def make_observation(self) -> str:
        """Create a single sample."""
        expr, symbol = self.make_eq(self.lhs_length)
        solution = list(solveset(expr, symbol))
        return f"Given {expr} = 0, solve for {symbol}.{self.sep}{symbol} is in {solution}"
