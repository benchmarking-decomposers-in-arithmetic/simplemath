"""The abstract base class underlying all the data makers."""
from abc import ABC, abstractmethod


class DataBase(ABC):
    """The abstract base class underlying all the data makers."""

    def __init__(self, num_observations):
        """Super().__init__ call will just do this."""
        self.num_observations = num_observations

    @abstractmethod
    def make_observation(self):
        """Create a single sample."""
        # pass

    @property
    def observations(self):
        """Return list of samples."""
        return [self.make_observation() for _ in range(self.num_observations)]
