"""Defining concrete validators."""
from typing import Optional, List, Dict
from .base import NumberValidator, Number


class Bounded(NumberValidator):
    """A bounded set."""

    def __init__(
            self,
            lower_bound: Optional[float] = None,
            upper_bound: Optional[float] = None) -> None:
        """Initialize a bounded set. In the case of both upper and lower bounds, it is compact."""
        assert lower_bound is not None or upper_bound is not None, \
            "Do not use this object to create an unbounded validator."
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.message = {
            (lower_bound, upper_bound): f"in [{self.lower_bound}, {self.upper_bound}]",
            (None, upper_bound): f"less than {self.upper_bound}",
            (lower_bound, None): f"greater than {self.lower_bound}",
        }

    def validate(self, value: Number):
        """Validate membership in a bounded set.

        This method implicitly validates isinstance(value, (float, int))
        because it will throw a TypeError on comparison
        """
        if (self.upper_bound is not None and value > self.upper_bound) \
           or (self.lower_bound is not None and value < self.lower_bound):
            raise ValueError(
                f"{value} needs to be {self.message[(self.lower_bound, self.upper_bound)]}."
            )


class BooleanValidator:
    """A string that means a boolean value."""

    def __init__(self, boolstrings: Dict[bool, List[str]]) -> None:
        """Send it a list of strings that you think are interpretable as booleans."""
        self.boolstrings = boolstrings

    def validate(self, value: str):
        """Validate that the value can be interpreted as a bool, and interpret it."""
        if value.lower() not in self.boolstrings[False] + self.boolstrings[True]:
            raise ValueError(
                f"{value} needs to be interpretable as a bool."
            )

    def __call__(self, value: str) -> bool:
        """Return a boolean if possible, otherwise raises."""
        self.validate(value)
        return value.lower() in self.boolstrings[True]
