"""
Design pattern via https://youtu.be/S_ipdVNSFlo?t=2153.

modified such that validators are _callable_
"""

from abc import ABC, abstractmethod
from typing import Union
Number = Union[float, int]


class NumberValidator(ABC):
    """A data validator, to be used for numbers."""

    def __call__(self, value: Number) -> Number:
        """Return value if validates. Validate will raise if fails."""
        self.validate(value)
        return value

    @abstractmethod
    def validate(self, value: Number):
        """Validate numbers."""
