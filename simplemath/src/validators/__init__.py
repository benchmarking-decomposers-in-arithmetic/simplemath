"""Validators defined here."""
from .validators import Bounded, BooleanValidator

Positive = Bounded(lower_bound=0.0)
LEqHundred = Bounded(upper_bound=100.0)
ReasonableExpBase = Bounded(lower_bound=2.0, upper_bound=32.0)
Observations = Bounded(lower_bound=0.0, upper_bound=100000.0)
GEqTwo = Bounded(lower_bound=2.0)
NumVariables = Bounded(lower_bound=1, upper_bound=15)
Boolean = BooleanValidator(
    {True: ["true", "yes", "y", "1", "t"], False: ["false", "no", "n", "0", "f"]}
)
