#!/usr/bin/env python
"""An API that generates and serves data for seq2seq training."""
from typing import Any, Dict, List, Optional, Tuple
from flask import Flask, request  # type: ignore
from src.classes import (  # type: ignore
    BinaryArithmeticData,
    LinearPolynomialData,
    ExpDecompArithmeticData,
    InequalityData,
    BinaryArithmeticInequalityData,
    SimpleMultiplicationData
)
from src.validators import (  # type: ignore
    Positive, LEqHundred, ReasonableExpBase, Observations, GEqTwo, NumVariables, Boolean
)


APP = Flask(__name__)


def validate_kwargs(
        kwargs: Dict[str, str],
        input_validators: Dict[str, Any]
) -> Optional[Tuple[Dict[str, List[str]], int]]:
    """Apply validators, perform type casting, etc.

    returning an error payload if anything goes wrong.
    """
    for key, value in kwargs.items():
        try:
            validator = input_validators[key]
            kwargs[key] = validator(value)
        except ValueError as vexc:
            return {
                "error": [f"{key} argument must obey. {vexc}"]
            }, 400
        except KeyError as kexc:
            return {
                "error": [f"{key} may have been misspelled. {kexc}"]
            }, 400

    if "low" in kwargs.keys() and "high" in kwargs.keys():
        low = kwargs["low"]
        high = kwargs["high"]
        if kwargs["low"] >= kwargs["high"]:
            return {
                "error": [f"low={low} must be lower than high={high}"]
            }, 400
    return None


@APP.route("/simplemultiplicationdata", methods=["GET"])
def simple_multiplication() -> Tuple[Dict[str, List[str]], int]:
    """Serve simple multiplication samples.

    EXAMPLE: 10*123<SEP>1230 or 3*4=12
    """
    input_validators = {
        "num_observations": lambda x: Observations(int(x)),
        "sep": str
    }
    if request.query_string:
        kwargs = {key: value
                  for key, value
                  in (keyval.split("=")
                      for keyval
                      in str(request.query_string, encoding="utf-8").split("&")
                  )
        }
    else:
        kwargs = dict()

    if "num_observations" not in kwargs.keys():
        kwargs["num_observations"] = "100"

    error = validate_kwargs(kwargs, input_validators)
    if error:
        return error

    smult = SimpleMultiplicationData(**kwargs)
    return {
        "observations": smult.observations
    }, 200


@APP.route("/binaryarithmeticdata", methods=["GET"])
def binary_arithmetic() -> Tuple[Dict[str, List[str]], int]:
    """Serve binary arithmetic samples.

    EXAMPLE: "Evaluate 635*244.<SEP>635*244=154940"
    """
    input_validators = {
        "num_observations": lambda x: Observations(int(x)),
        "sep": str,
        "low": int,
        "high": int,
        "omit_division": Boolean,
        "omit_multiplication": Boolean,
        "omit_subtraction": Boolean,
        "tearse": Boolean
    }
    if request.query_string:
        kwargs = {key: value
                  for key, value
                  in [keyval.split("=")
                      for keyval
                      in str(request.query_string, encoding="utf-8").split("&")
                  ]
        }
    else:
        kwargs = dict()

    if "num_observations" not in kwargs.keys():
        kwargs["num_observations"] = "100"

    error = validate_kwargs(kwargs, input_validators)
    if error:
        return error

    arith = BinaryArithmeticData(**kwargs)
    return {
        "observations": arith.observations
    }, 200


@APP.route("/inequalitydata", methods=["GET"])
def inequality() -> Tuple[Dict[str, List[str]], int]:
    """Serve binary arithmetic samples.

    EXAMPLE: "Evaluate 635*244.<SEP>635*244=154940"
    """
    input_validators = {
        "num_observations": lambda x: Observations(int(x)),
        "sep": str,
        "low": int,
        "high": int,
    }
    if request.query_string:
        kwargs = {key: value
                  for key, value
                  in [keyval.split("=")
                      for keyval
                      in str(request.query_string, encoding="utf-8").split("&")
                  ]
        }
    else:
        kwargs = dict()

    if "num_observations" not in kwargs.keys():
        kwargs["num_observations"] = "100"

    error = validate_kwargs(kwargs, input_validators)
    if error:
        return error

    ineqs = InequalityData(**kwargs)
    return {
        "observations": ineqs.observations
    }, 200


@APP.route("/binaryarithmeticinequalitydata", methods=["GET"])
def binary_arithmetic_inequality() -> Tuple[Dict[str, List[str]], int]:
    """Serve binary arithmetic samples.

    EXAMPLE: "Evaluate 635*244.<SEP>635*244=154940"
    """
    input_validators = {
        "num_observations": lambda x: Observations(int(x)),
        "sep": str,
        "low": int,
        "high": int,
        "omit_division": Boolean,
    }
    if request.query_string:
        kwargs = {key: value
                  for key, value
                  in [keyval.split("=")
                      for keyval
                      in str(request.query_string, encoding="utf-8").split("&")
                  ]
        }
    else:
        kwargs = dict()

    if "num_observations" not in kwargs.keys():
        kwargs["num_observations"] = "100"

    error = validate_kwargs(kwargs, input_validators)
    if error:
        return error

    ineq = BinaryArithmeticInequalityData(**kwargs)
    return {
        "observations": ineq.observations
    }, 200


@APP.route("/expdecompdata", methods=["GET"])
def exp_decomp() -> Tuple[Dict[str, List[str]], int]:
    """Serve samples of decomposing integers by exponential bases.

    EXAMPLE: "Decompose 254 in base 10<SEP>254=2(10^2)+5(10^1)+4(10^0)"
    """
    input_validators = {
        "base": lambda x: ReasonableExpBase(int(x)),
        "num_observations": lambda x: Observations(int(x)),
        "low": lambda x: GEqTwo(int(x)),
        "high": lambda x: GEqTwo(int(x)),
        "sep": str
    }
    if request.query_string:
        kwargs = {key: value
                  for key, value
                  in [keyval.split("=")
                      for keyval
                      in str(request.query_string, encoding="utf-8").split("&")
                  ]
        }
    else:
        kwargs = dict()

    if "num_observations" not in kwargs.keys():
        kwargs["num_observations"] = "100"
    if "base" not in kwargs.keys():
        kwargs["base"] = "10"

    error = validate_kwargs(kwargs, input_validators)
    if error:
        return error

    expdecomp = ExpDecompArithmeticData(**kwargs)
    return {
        "observations": expdecomp.observations
    }, 200


@APP.route("/linearpolynomialdata", methods=["GET"])
def linear_polynomial() -> Tuple[Dict[str, List[str]], int]:
    """Serve linear polynomials.

    EXAMPLE: "Given 414*k + 751*s = 0, solve for k.<SEP>k is in [-751*s/414]"
    """
    input_validators = {
        "num_observations": lambda x: LEqHundred(Positive(int(x))),
        "lhs_length": lambda x: NumVariables(int(x)),
        "sep": str,
        "low": int,
        "high": int
    }
    if request.query_string:
        kwargs = {key: value
                  for key, value
                  in [keyval.split("=")
                      for keyval
                      in str(request.query_string, encoding="utf-8").split("&")
                  ]
        }
    else:
        kwargs = dict()

    if "num_observations" not in kwargs.keys():
        kwargs["num_observations"] = "100"
    if "lhs_length" not in kwargs.keys():
        kwargs["lhs_length"] = "4"

    error = validate_kwargs(kwargs, input_validators)
    if error:
        return error

    alg = LinearPolynomialData(**kwargs)
    return {
        "observations": alg.observations
    }, 200


def start():
    """Run simplemath in dev mode."""
    APP.run()
