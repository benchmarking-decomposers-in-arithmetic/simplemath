FROM python:3.7.7-slim-buster
WORKDIR /app
COPY README.md .
COPY requirements.txt requirements.txt
COPY setup.py .
COPY simplemath simplemath
RUN pip install .
CMD gunicorn simplemath:APP --bind 0.0.0.0:$PORT
