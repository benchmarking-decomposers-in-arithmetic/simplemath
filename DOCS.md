# simplemath -- Documentation

## base url: `simplemath1.herokuapp.com/`

## Each endpoint takes `GET` requests with _optional_ querystring params

- Endpoint: `binaryarithmeticdata`
- Description: strings of the form `"Evaluate 635*244.<SEP>635*244=154940"`
- Params: 

| name                  |               type           | default |
|-----------------------|------------------------------|---------|
| num_observations      | integer between 0 and 100000 | 100     |
| sep                   | string                       | <SEP>   |
| low                   | integer                      | 0       |
| high                  | integer                      | 1000    |
| `omit_division`       | boolean string               | True    |
| `omit_multiplication` | boolean string               | False   |
| `omit_subtraction`    | boolean string               | False   |

- Endpoint: `expdecompdata`
- Description: strings of the form `"Decompose 254 in base
  10<SEP>254=2(10^2)+5(10^1)+4(10^0)"`
- Params: 

| name             |               type           | default |
|------------------|------------------------------|---------|
| base             | integer between 2 and 32     | 10      |
| num_observations | integer between 0 and 100000 | 100     |
| sep              | string                       | <SEP>   |
| low              | integer                      | 0       |
| high             | integer                      | 1000    |


- Endpoint: `linearpolynomialdata`
- Description: strings of the form `Given 414*k + 751*s = 0, solve for k.<SEP>k is in [-751*s/414].`
- Params: 

| name               |               type           | default |
|--------------------|------------------------------|---------|
| `lhs_length`       | integer between 1 and 15     | 4       |
| `num_observations` | integer between 0 and 100    | 100     |
| sep                | string                       | <SEP>   |
| low                | integer                      | 0       |
| high               | integer                      | 1000    |

Notice the `num_observations` is throttled lower. This has to do with compute
times. Heroku has a 30 second timeout. 

- Endpoint: `simplemultiplicationdata`
- Description: strings of the form `"10*123<SEP>1230"` or `"3*4<SEP>12"`
- Params: 

| name             |               type           | default |
|------------------|------------------------------|---------|
| num_observations | integer between 0 and 100000 | 100     |
| sep              | string                       | <SEP>   |

- Endpoint: `inequalitydata`
- Description: strings of the form `4<5<SEP>True`
- Params: 

| name               |               type           | default |
|--------------------|------------------------------|---------|
| `num_observations` | integer between 0 and 100000 | 100     |
| sep                | string                       | <SEP>   |
| low                | integer                      | 0       |
| high               | integer                      | 1000    |

- Endpoint: `binaryarithmeticinequalitydata`
- Description: strings of the form `4+5<6*7<SEP>True`
- Params: 

| name               |               type           | default |
|--------------------|------------------------------|---------|
| `num_observations` | integer between 0 and 100000 | 100     |
| sep                | string                       | <SEP>   |
| low                | integer                      | 0       |
| high               | integer                      | 1000    |
| `omit_division`    | boolean string               | True    |
